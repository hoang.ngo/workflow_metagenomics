#!/bin/bash
export PYTHONPATH=''
source /usr/local/genome/Anaconda2-5.1.0/etc/profile.d/conda.sh
conda activate snakemake-5.7.4

mkdir -p LOGS/snakemake/ LOGS/err/ LOGS/out/

snakemake \
--snakefile $1 \
--jobscript ../workflow_metagenomics/jobscript.sh \
--cluster-config ../workflow_metagenomics/cluster.json \
--cluster "qsub -V -cwd -R y -N {rule} -o {cluster.out} -e {cluster.err} -q {cluster.queue} -pe thread {threads} {cluster.cluster}" \
--keep-going \
--jobs 80 \
--wait-for-files \
--latency-wait 150 \
--verbose \
--printshellcmds
