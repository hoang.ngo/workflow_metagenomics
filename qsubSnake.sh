mkdir -p LOGS/snakemake
qsub -V -cwd -o LOGS/snakemake/ -e LOGS/snakemake/ -q infinit.q -N `basename $1` ../workflow_metagenomics/RunSnake.sh $1
